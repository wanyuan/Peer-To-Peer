module.exports = {
  router: null,
  setRouter: function(router){
      this.router = router;
  },
  getRouter: function(){
      return this.router;
  }
};