var React = require('react');
var _ = require('lodash');
var classnames = require('classnames');

var ModuleConstant = require('../../../constants/ModuleConstant');
var ModuleActions = require('../../../actions/ModuleActions');

var ModuleStore = require('../../../stores/ModuleStore');

module.exports = React.createClass({
    gotoModule: function(moduleId){
        ModuleActions.gotoModule(moduleId);
    },

    render: function(){
        var that = this;
        var modules = _.chain(ModuleConstant.modules).values().filter(function(module){
            return module.id !== 'home';
        }).map(function(module){
            var id = module.id;
            var classStr = classnames('module-icon', id);
            return (
              <div key={id} className={classStr} onClick={that.gotoModule.bind(that, id)}>
                  <div className='bg-layout'></div><div className='icon'></div><span>{module.label}</span>
              </div>
            );
        }).value();

        return (
            <div className="module-icon-container-wrap vertical-middle-sm">
                <div className="module-icon-container">
                  {modules}
                </div>
            </div>
        );
    }
});