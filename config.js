var config = {};

//file upload config
config.cache_dir  = './cache/';
config.upload_dir = './upload/';
config.allow_upload_type = {
	'image/jpeg' : true
};

module.exports = config;